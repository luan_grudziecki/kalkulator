program Calculator;

uses
  {$IFDEF EurekaLog}
  EMemLeaks,
  EResLeaks,
  EDebugExports,
  EDebugJCL,
  EFixSafeCallException,
  EMapWin32,
  ExceptionLog7,
  {$ENDIF EurekaLog}
  Vcl.Forms,
  Form_Main in 'Form_Main.pas' {frmMain},
  uCalculations in 'uCalculations.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

