unit Form_Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.StdCtrls, cxTextEdit, cxCurrencyEdit;

type
  TfrmMain = class(TForm)
    ceValue1: TcxCurrencyEdit;
    ceValue2: TcxCurrencyEdit;
    btnCalculate: TButton;
    ceResult: TcxCurrencyEdit;
    procedure btnCalculateClick(Sender: TObject);
  private
  public
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  uCalculations;

procedure TfrmMain.btnCalculateClick(Sender: TObject);
begin
  ceResult.Value := uCalculations.Add(ceValue1.Value, ceValue2.Value);
end;

end.
