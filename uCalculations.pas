unit uCalculations;

interface

function Add(Value1, Value2 : Currency) : Currency;
function Substract(Value1, Value2 : Currency) : Currency;


implementation

function Add(Value1, Value2 : Currency) : Currency;
begin
  Result := Value1 + Value2;
end;

function Substract(Value1, Value2 : Currency) : Currency;
begin
  Result := Value1 - Value2;
end;

end.
