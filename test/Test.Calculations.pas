unit Test.Calculations;

interface
uses
  DUnitX.TestFramework,
  uCalculations;

type

  [TestFixture]
  TTestCalculations = class(TObject)
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure Test1;
    // Test with TestCase Atribute to supply parameters.
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);

    [Test]
    [TestCase('Test_Add', '1,2,3')]
    procedure Test_Add(const AValue1, AValue2, AValue3 : Extended);

    [Test]
    procedure Test_Currency;
  end;

implementation

procedure TTestCalculations.Setup;
begin
end;

procedure TTestCalculations.TearDown;
begin
end;

procedure TTestCalculations.Test1;
begin
  Assert.AreEqual(1, 1);
end;

procedure TTestCalculations.Test2(const AValue1 : Integer;const AValue2 : Integer);
begin
  Assert.AreEqual(AValue1, AValue1);
end;

procedure TTestCalculations.Test_Add(const AValue1, AValue2, AValue3 : Extended);
begin
  Assert.AreEqual(AValue3, AValue1 + AValue2);
end;

procedure TTestCalculations.Test_Currency;
const
  EXPECTED : Currency = 7.60;
  VALUE_1  : Currency = 3.10;
  VALUE_2  : Currency = 4.50;
begin
  Assert.AreEqual(EXPECTED, uCalculations.Add(VALUE_1, VALUE_2));
end;

initialization
  TDUnitX.RegisterTestFixture(TTestCalculations);
end.
