object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Calculator'
  ClientHeight = 242
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ceValue1: TcxCurrencyEdit
    Left = 8
    Top = 10
    EditValue = 10.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    TabOrder = 0
    Width = 97
  end
  object ceValue2: TcxCurrencyEdit
    Left = 111
    Top = 10
    EditValue = 15.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    TabOrder = 1
    Width = 97
  end
  object btnCalculate: TButton
    Left = 224
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Calculate'
    TabOrder = 2
    OnClick = btnCalculateClick
  end
  object ceResult: TcxCurrencyEdit
    Left = 307
    Top = 10
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    TabOrder = 3
    Width = 97
  end
end
